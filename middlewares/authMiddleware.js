module.exports = (req,res,next) => {
    if(req.session.userId){
        // ok
        next()
    }else{
        res.redirect('/auth/login')
    }
}