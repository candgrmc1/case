FROM node:12.18-alpine
ENV NODE_ENV=dev
WORKDIR /usr/app
COPY ["./package.json", "./package-lock.json*", "./"]
RUN npm install  
RUN npm i -g nodemon
COPY . .
EXPOSE 3300
CMD ["nodemon", "./app.js"]
