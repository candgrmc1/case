module.exports = {
    name: String,
    surname: String,
    email: {
        type: String,
        index: {
            unique: true
        }
    },
    password: String,
    lang: String,
    country: String,
    createdAt: {
        type: Number,
        default: Date.now().valueOf()
    },
}