module.exports = {
    related: {
        type: String,
        index: true
    },
    type: String,

    createdAt: {
        type: Number,
        default: Date.now().valueOf()
    }
}