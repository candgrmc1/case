module.exports = {
    ...require('./factories'),
    ...require('./tools')
}