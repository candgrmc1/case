"use strict"
const colors = require('colors')
colors.setTheme({
    info: 'cyan',
    warn: 'yellow',
    success: 'green',
    error: 'red'
});

class Log {
    source
    constructor({
        source,
    }) {

        return (() => {
            this.source = source
            return this
        })()
    }



    success = (...logs) => {
        logs.map( log => {
            const message = this.source ? `${this.source}::${log}`.success : log.success.bold
            console.log(message)
        })
        
    }

    error = (...logs) => {
        logs.map( log => {
            const message = this.source ? `${this.source}::${log}`.error.bold : log.error.bold
            console.log(message)
        })
        
    }

    warn = (...logs) => {
        logs.map( log => {
            const message = this.source ? `${this.source}::${log}`.warn.bold : log.warn.bold
            console.log(message)
        })
        
    }

    trace = (...logs) => {
        logs.map( log => {
            const message = this.source ? `${this.source}::${log}`.info.bold : log.info.bold
            console.log(message)
        })
        
    }
   
}

module.exports = Log