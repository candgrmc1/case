var rand = function() {
    return Math.random().toString(36).substr(2); // remove `0.`
};

module.exports ={
    rand
}