module.exports = ({
    routes,
    router,
    disco,
    middlewares: {
        authMiddleware
    }
}) => {

    for(let {method, handle, auth, path} of routes){

        // inject middleware
        if(auth){
            console.log(auth,path)
            router[method]( path, (req,res,next) => authMiddleware(req,res,next) ) 
        }
        
        router[method]( path ,async (req,res) => {
            const query = req.queryString
            const body = req.body
            await handle(req,res, {
                disco
            }, query || body )
        } )
    }

    return router
}