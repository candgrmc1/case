
module.exports =  {
    ...require('./service'),
    ...require('./router'),
    ...require('./db'),
    createApp: require('./createApp'),
    createSocket: require('./createSocket')
}