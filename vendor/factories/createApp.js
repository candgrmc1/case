const createModel = require("./db/createModel")

const DISCOVERY_PATH = './service/DiscoveryService'
module.exports = async ({
    layers: {
        app,
        http,
        middlewares,
        services,
    },
    factories: {
        createService,
        createSocket,
        createModel,
        Log
    },
    db:{
        mongoose,
        models:schemas
    },
    router: {
        router,
        configure
    },
    routes
}) => {
    const log = new Log({
        source: 'application'
    })
    const discoveryService = await createService({
        name: 'DiscoveryService',
        service: require(DISCOVERY_PATH),
        Log
    })
    const io = require('socket.io')(http);
    await createSocket({io, discoveryService})

    const run = async () => {
        const models = {}
        for(let name in schemas ){
            models[name] = await createModel(name,schemas[name],mongoose)
        }

        

        // configure services

        for (let name in services) {
            const service = await createService({
                name,
                service: services[name],
                Log,
                discoveryService,
                models
            })

            discoveryService.push(name, service)

        }

        for (let base in routes) {
            const route = await configure({
                routes: routes[base],
                disco: discoveryService,
                router,
                middlewares

            })

            app.use(base, route)
        }

        
    }

    return {
        application: {
            run
        },
        discoveryService
    }

}