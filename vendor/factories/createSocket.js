
const {rand} = require('../tools/rand')
module.exports = ({io, discoveryService}) => {
    io.on('connection', (socket) => {
        console.log('user connected')
        const pubsubService = discoveryService.get('PubSubService')
        pubsubService.setSocket(socket)
        
        socket.on('public-chat',async (body) => {
            const {msg,sentBy} = body
            const token = rand() + rand()
            const cleanMsg = msg.replace(/<\/?[^>]+(>|$)/g, "")
            await pubsubService.publish({
                channel: 'public-chat-publish',
                msg: cleanMsg,
                sentBy,
                storeHistory: false,
                token
            })
        }) 
        
        socket.on('disconnect',() => {
            console.log('user disconnected')
        })
    });
}