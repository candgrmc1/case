class DiscoveryService {
    services = {}
    push(key,service){
        this.services[key] = service
    }

    get(name){
        const service = this.services[name]

        if(service == undefined){

            this.log.error('service-not-found')
            throw new Error('service-not-found')
        }

        return service
    }
}

DiscoveryService.prototype.log = undefined

module.exports = DiscoveryService