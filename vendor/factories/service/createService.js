const createService = async ({
    name,
    service,
    Log,
    config,
    discoveryService,
    models
    
}) => {
    const log = new Log({source: 'create-service'})
   
    
    const protos = Object.getOwnPropertyNames(service.prototype)
    for (let prototype of protos) {
        
        switch (prototype) {

            case 'log':
                let l = new Log({source:name})
                service.prototype.log = l
                break

            case 'config':
                if (config == undefined) {
                    console.log('config-not-found')
                    log.error('config-not-found')
                    process.exit(0);
                }
                if (config[name] == undefined) {
                    console.log('service-config-not-found')
                    log.error('service-config-not-found')
                    process.exit(0);
                }
                service.prototype.config = config[name]
                break

            default:

                break
        }
    }

    service.prototype.discoveryService = discoveryService
    service.prototype.models = models

    log.trace(`service ${name} is configured`)
    return new service
}

module.exports = createService
