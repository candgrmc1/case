"use strict"
const {
    createService,
    createApp,
    createSocket,
    createModel,
    configure,
    Log
} = require('./vendor')

const middlewares = require('./middlewares')

const env = process.env.NODE_ENV || 'dev'
const config = require('./config')[env] 

var bodyParser = require('body-parser')
const express = require('express')
const app = express();
const http = require('http').createServer(app);

const ejs = require('ejs');
const session = require('express-session')

app.set('views',__dirname+'/views');
app.engine('.ejs', ejs.__express);
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(session({
    secret: config.sessionSecret,
    resave: false,
    saveUninitialized: true
}))
app.use(bodyParser.urlencoded({ extended: false }))

const router = express.Router()
const mongoose = require('mongoose');
const connectionString = process.env.DATABASE_CONNECTIONSTRING || 'mongodb://localhost:27017/case'
mongoose.connect( connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


const log = new Log({source:'application'})

createApp({
    layers: {
        http,
        app,
        middlewares,
        services : require('./services')
    },
    factories: {
        createService,
        createSocket,
        createModel,
        Log
    },
    router:{
        router,
        configure
    },
    db:{
        mongoose,
        models: require('./models')
    },
    
    routes: require('./routes')
}).then(async ({application,discoveryService}) =>{
    

    await application.run()
    
    http.listen(config.PORT, () => log.success(`app is running on ${config.PORT}`) )
} )

