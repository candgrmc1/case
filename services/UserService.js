class UserService {
    User = this.models.User
    async getAll(){
        return await this.User.find({},{ name: 1, surname: 1,email: 1 })
    }

    async getById(id){
        return await this.User.findById(id,{ name: 1, surname: 1,email: 1 })
    }
}

UserService.prototype.log = undefined
UserService.prototype.models = undefined
UserService.prototype.discoveryService = undefined

module.exports = UserService