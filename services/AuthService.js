const bcrypt = require('bcryptjs')
const saltRounds = 10;
const {rand} = require('../vendor')


async function hash(str){
    return new Promise((resolve,reject) => {
        bcrypt.hash(str, saltRounds, function(err, hash) {
            // Store hash in your password DB.
            if(err) reject(err)

            resolve(hash)
        });
    })
}

async function compare(str,hash){
    return new Promise((resolve,reject) => {
        bcrypt.compare(str, hash, function(err, result) {
            resolve(result)
        });
    })
}


class AuthService {
    User = this.models.User
    Login = this.models.Login

    async login(email,password){
        const user = await this.User.findOne({ email }).exec();

        if(user == undefined){
            return false
        }
        
        const comp = await compare(password,user.password)

        if(!comp){
            return false
        }

        const log = new this.Login({
            type: 'login',
            related: user._id,
        })
        log.save()

        const token = rand() + rand()
        
        const pubsubService = this.discoveryService.get('PubSubService')
        await pubsubService.publish({
            channel: 'login-publish',
            msg: `${user.name} ${user.surname} is logged in`, 
            sentBy: user._id, 
            storeHistory: false,
            token

        })
        return user._id
    }

    async register({
        name,
        surname,
        email,
        password,
        lang,
        country
    }) {
        const user = new this.User({
            name,
            surname,
            email,
            password: await hash(password),
            lang,
            country
        })
        user.save()
        const token = rand() + rand()
        const pubsubService = this.discoveryService.get('PubSubService')
        await pubsubService.publish({
            channel: 'register-publish',
            msg: `${user.name} ${user.surname} is registered`, 
            sentBy: user._id, 
            storeHistory: false,
            token

        })

        return true

    }

    async logout(req){
        const token = rand() + rand()
        const pubsubService = this.discoveryService.get('PubSubService')
        await pubsubService.publish({
            channel: 'register-publish',
            msg: `${user.name} ${user.surname} is registered`, 
            sentBy: user._id, 
            storeHistory: false,
            token

        })
        req.session.destroy()

    }
}

AuthService.prototype.log = undefined
AuthService.prototype.models = undefined
AuthService.prototype.discoveryService = undefined

module.exports = AuthService