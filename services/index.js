module.exports  = {
    ChatService: require('./ChatService'),
    PubSubService: require('./PubSubService'),
    AuthService: require('./AuthService'),
    UserService: require('./UserService')
}