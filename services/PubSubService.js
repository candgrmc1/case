
class PubSubService {
    socket

    // must be in redis
    map = {
        'public-chat':[]
    }


    setSocket(s){
        this.socket = s
        this.socket.on('Ping',(s) => {
            console.log('ping..')
            
        })
    }


    subscribe(user,channel){
        const set = this.map[channel]

        if(set == undefined){
            this.log.error(`channel (${channel}) is not found`)
            // throw error 
            return 
        }
        set.push(user)
    }

    unsubscribe(user,channel){
        const set = this.map[channel]

        if(set == undefined){
            this.log.error(`channel (${channel}) is not found`)
            // throw error 
            return 
        }
        const index = set.indexOf(user)
        if(index > -1) {
            set.splice(index,1)
        }else{
            this.log.warn(`user (${user}) is not subscribing this channel (${channel})`)
        }
    }
    
    async publish({channel, msg, sentBy, storeHistory,token}){
        if(storeHistory){
            // store process
        }
        this.socket.emit(channel,{msg, token, sentBy})
    }
}
PubSubService.prototype.discoveryService = undefined
PubSubService.prototype.log = undefined

module.exports = PubSubService