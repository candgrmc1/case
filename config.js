module.exports = {
    'dev': {
        PORT: 3300,
        sessionSecret: 'dev-secret-key'
    },
    'prod': {
        PORT: 3300,
        sessionSecret: 'prod-secret-key'
    }
}