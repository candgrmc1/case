module.exports = [
    {
        path: '/',
        auth: true,
        method: 'get',
        handle: async (req,res, {disco}) => {
            const pubsub = disco.get('PubSubService')
            const userService = disco.get('UserService')
            const user = await userService.getById(req.session.userId)
            // test warning case
            pubsub.subscribe('test1','public-chat')
            pubsub.unsubscribe('test','public-chat')
            // -- test warning case
            res.render('./index',{fullname:`${user.name} ${user.surname}`, userId: user._id})
        }
    }
]