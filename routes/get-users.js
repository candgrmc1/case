module.exports = [
    {
        path: '/get-users',
        auth: true,
        method: 'get',
        handle: async (req,res, {disco}) => {
            const userService = disco.get('UserService')
            const users = await userService.getAll()
            
            res.json({
                success: true,
                data:users
            })

        }
    }
]