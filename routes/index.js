module.exports = {
    '/home': [...require('./home')],
    '/auth': [...require('./auth')],
    '/': [
        ...require('./get-user'),
        ...require('./get-users')
    ],
}
    
