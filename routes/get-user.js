module.exports = [
    {
        path: '/get-user/:id',
        auth: true,
        method: 'get',
        handle: async (req,res, {disco},body) => {
            const userService = disco.get('UserService')
            const {id} = body
            const user = await userService.getById(id)
            
            res.json({
                success: true,
                ddata:user
            })

        }
    }
]