module.exports = [
    ...require('./login'),
    ...require('./logout'),
    ...require('./register')
]