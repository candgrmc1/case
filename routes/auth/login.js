module.exports = [
    {
        path: '/login',
        auth: false,
        method: 'get',
        handle: async (req,res, {disco}) => {
            
            res.render('./login')
        }
    },
    {
        path: '/login',
        auth: false,
        method: 'post',
        handle: async (req,res, {disco}, body) => {
            const authService = disco.get('AuthService')
            const {email,password} = body
            const userId = await authService.login(email,password)
            req.session.userId = userId
            res.redirect('/home')
        }
    }
]