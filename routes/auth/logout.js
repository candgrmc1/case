module.exports = [
    {
        path: '/logout',
        auth: false,
        method: 'get',
        handle: async (req,res, {disco}) => {
            const authService = disco.get('AuthService')

            await authService.logout(req)
            res.redirect('/auth/login')
        }
    },
    
]