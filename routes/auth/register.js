
module.exports = [
    {
        path: '/register',
        auth: false,
        method: 'get',
        handle: async (req,res, {disco}) => {
            
            res.render('./register')
        }
    },
    {
        path: '/register',
        auth: false,
        method: 'post',
        handle: async (req,res, {disco}, body) => {
            const authService = disco.get('AuthService')
            const {
                name,
                surname,
                email,
                password,
                lang,
                country
            } = body

            await authService.register({
                name,
                surname,
                email,
                password,
                lang,
                country
            })
            

            res.redirect('/auth/login')
        }
    }
]